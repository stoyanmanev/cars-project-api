const express = require("express");
const cars = require("../firebase/cars/cars");

const router = express.Router();

router.get("/test", async function (_, res) {
  res.json({ success: true, message: "Dev" });
});

router.get("/cars", async function (req, res) {
  const queries = req?.query ? req.query : {};
  const response = await cars("list", queries);

  res.json(response);
});

module.exports = router;
