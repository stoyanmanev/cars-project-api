// Import the functions you need from the SDKs you need
const { initializeApp } = require("firebase/app");

function initializeFirebase(){
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries
    
    // Your web app's Firebase configuration
    const firebaseConfig = {
      apiKey: "AIzaSyBtBJ29fq3CiI1eTkEG-WEtn7ZeqYp_MfY",
      authDomain: "cars-db-8a83e.firebaseapp.com",
      databaseURL: "https://cars-db-8a83e-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "cars-db-8a83e",
      storageBucket: "cars-db-8a83e.appspot.com",
      messagingSenderId: "709761946020",
      appId: "1:709761946020:web:bc91aed3dca3d49c447131"
    };
    
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    return app;

}

module.exports = initializeFirebase
