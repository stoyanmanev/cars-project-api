const { getDatabase, ref, child, get } = require("firebase/database");
const { carsList, useQueries } = require("./helpers");

async function fetchCars() {
  const dbRef = ref(getDatabase());
  const response = await get(child(dbRef, `cars/`))
    .then((snapshot) => {
      if (snapshot.exists()) {
        return {
          success: true,
          message: "Cars Response",
          data: snapshot.val(),
        };
      } else {
        throw new Error("No data available");
      }
    })
    .catch((error) => {
      return { success: false, message: error.message, data: {} };
    });
  return response;
}

/**
 * @params
 * responseType?: string -> type of returned structure of cars,
 * responseType: 'list' -> return a list of all cars in database, used on default
 * queries: object -> queries from url
 */

async function cars(responseType, queries) {
  const type = responseType ? responseType : "list";
  try {
      const response = await fetchCars();
    
      if (!response.success) {
        return response;
      }
    
      if (Object.keys(queries).length !== 0) {
        const queriesData = useQueries(response.data, queries);
        if(Object.keys(queriesData).length > 0){
            response.data = queriesData;
        }else{
            throw new Error('Result with this filter not found', response.data);
        }
      }
    
      if (type === "list") {
        const responseList = carsList(response.data);
        const mergeResponse = { ...response, data: responseList };
        return mergeResponse;
      }
    
  } catch (error) {
    const message = error?.message ? error.message : 'Something went wrong';
    return {success: false, message, data: []}
  }
}

module.exports = cars;
