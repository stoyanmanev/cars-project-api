/**
 * @params
 * cars: object -> includes cars and return values of each object,
 */

function carsList(cars) {
  const values = Object.values(cars);
  return values;
}


/**
 * @params
 * cars: object of Cars
 * queries: object -> queries from url
 */

function useQueries(cars, queries) {
  const entries = Object.entries(cars);
  const queriesList = Object.entries(queries);
  const filteredList = entries.filter((car) => {
    const [_, valueE] = car;
    const filter = queriesList.map((query) => {
      const [key, value] = query;

      // if car object includes query params return

      if (valueE[key] && valueE[key].toLowerCase() === value.toLowerCase()) {
        return true;
      } else {
        return false;
      }
    });
    if (!filter.includes(false)) {
      return car;
    }
  });

  const convertToInputFormat = Object.fromEntries(filteredList);
  return convertToInputFormat;
}

module.exports = {
  carsList,
  useQueries,
};
